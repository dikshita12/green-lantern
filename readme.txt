=== Green-Lantern ===
Contributors: weblizar
Tags: two-columns, three-columns, four-columns, custom-menu, right-sidebar, custom-background, featured-image-header, sticky-post, theme-options, threaded-comments, featured-images, flexible-header, translation-ready ,  custom-logo , news
Requires at least: 4.9
Tested up to: 5.5
Stable tag: 4.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Green Lantern is a fully responsive multipurpose WordPress theme. 

== Description ==
Green Lantern theme is responsive and compatible with the latest version of WordPress. This theme is a perfect choice for a travel agency website, hotels and travel agencies review blog, traveling worldwide and tourism blog, travel agents site, tour operator or tourism company website, etc. Green Lantern has custom menus support to choose the menu in Primary Location that is in Header of the site.
<a href="https://wordpress.org/themes/green-lantern/">Discover more about the theme</a>.

== Frequently Asked Questions ==

= Where can i raise the theme issue? =
Please drop your issues here <a href="https://wordpress.org/support/theme/green-lantern"> we'll try to triage issues reported on the theme forum, you'll get a faster response.

= Where can I read more about Green-Lantern? =
<a href="https://wordpress.org/themes/green-lantern/">Theme detail page</a>

-------------------------------------------------------------------------------------------------
License and Copyrights for Resources used in Green-lantern WordPress Theme
-------------------------------------------------------------------------------------------------

1) Bootstrap CSS
=================================================================================================
Bootstrap v3.1.1 (http://getbootstrap.com)
Copyright 2011-2014 Twitter, Inc.
Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

2) Font Awesome
=================================================================================================
Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)

Font Awesome Free 5.11.2 by @fontawesome - https://fontawesome.com
License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)

3) Animate CSS
=================================================================================================
Animate.css - http://daneden.me/animate
Licensed under the MIT license
Copyright (c) 2013 Daniel Eden

4) Swiper
=================================================================================================
Swiper 3.3.1
http://www.idangero.us/swiper/
Copyright 2016, Vladimir Kharlampidi
The iDangero.us
http://www.idangero.us/
Licensed under MIT

5) Revolution Slider
=================================================================================================
version:   	2.1
author:		themepunch
Licensed under MIT

6) Bootstrap Js
=================================================================================================
Bootstrap v3.3.2 (http://getbootstrap.com)
Copyright 2011-2015 Twitter, Inc.
Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

7) Holder Js
=================================================================================================
Holder - client side image placeholders
Version 2.9.0+f2dkw
© 2015 Ivan Malopinsky - http://imsky.co
Site:     http://holderjs.com
Issues:   https://github.com/imsky/holder/issues
License:  MIT

8) CarouFredSel Js
=================================================================================================
jQuery carouFredSel 6.2.1
Demo's and documentation:
caroufredsel.dev7studios.com
Copyright (c) 2013 Fred Heusschen
www.frebsite.nl
Dual licensed under the MIT and GPL licenses.

9) Themepunch Js
=================================================================================================
jQuery Transit - CSS3 transitions and transformations
Copyright(c) 2011 Rico Sta. Cruz <rico@ricostacruz.com>
MIT Licensed.
Version:1.0

10) Themepunch Revolution Js
=================================================================================================
jquery.themepunch.revolution.js - jQuery Plugin for kenburn Slider
version: 3.0 (16.06.2013)
requires jQuery v1.7 or later (tested on 1.9)
author ThemePunch

== TGMPA ==
	Source : https://github.com/TGMPA/tgm-example-plugin/blob/master/LICENSE

== Screenshots ==

Slider image--
Source:https://pxhere.com/en/photo/1206902

Copyright 2019, Pxhere
https://pxhere.com
Pxhere provides images under license
license: https://pxhere.com/en/license

== Changelog ==

= 4.0 =
* Add customizer into plugin
* Add Accessebility
* Update font-awesome


= 3.3.33 =
**Minor bug fix


= 3.3.32 =
* Remove Inline CSS & Js
* Create home template
* Screenshot updated
* Minor CSS corrections.
* Changes in theme info notice.

= 3.3.3 =
* Font Awesome Updated
* Minor Corrections in header design.

= 3.3.2 =
* Changes in admin notice
* Tested with lastest version of WordPress

= 3.3.1 =
* CSS bundled.

= 3.3.0 =
* Added new fullpage template

= 3.2.0 =
* Default Menu Issue fix.

= 3.1.9 =
* Scroll-up button added.
* Tested with latest WP.
* Minor CSS issue fixed.

= 3.1.8 =
* icon picker updated with latest version.
* Changes in theme info notice.

= 3.1.7 =
* Readme.text file changed as per new rule.
* Theme Welcome page added.
* Minor issues fixed for customizer.

= 3.1.6 =
* Screenshot updated.

= 3.1.5 =
* Rating options added on admin panel.

= 3.1.4 =
* Go Green-lantern premium options added on admin panel.

= 3.1.3 =
* Search form added for home page.
* Post Format added.

= 3.1.2 =
* Background-color option added.
* Category option added in theme general option.
* Documentation added.

= 3.1.1 =
* New slider added(Touch Slider).

= 3.1 =
* Snow effect option added.
* Rating banner dismiss option added. 

= 3.0 =
* Added Icon Picker.
* Added WP Editor. 
* Added Section Drag-Drop.
* Added 5-Star Rating Option.

= 2.9 =
* Excerpt option added.
* Minor changes in header.

= 2.8 =
* Snow effect removed.
* Minor changes in custom header.

= 2.7 =
* Comment func() updated.
* Banner removed.

= 2.6 =
* Update Pro Theme & Plugin Page with Upcoming Premium Themes.
* Font Awesome new version 4.7.0 added.
* Custom header Added.
* Snow effect added for customizer.

= 2.5 =
* Update Pro Theme & Plugin Page with Upcoming Premium Themes.
* Font Awesome new version added.
* Plugin Recomendation added.

= 2.4 =
* Update Theme Info Page with Premium Themes and Plugins Features.

= 2.3.1 =
* Update Theme Info Page with Premium Themes and Plugins Features and Home Page Configuration.
* Google Font Issue Fixed.

= 2.3 =
* Slider issue fixed with WP-4.5

= 2.2 =
* Google Font API Added.
* Update Theme Info Page with Premium Theme Features and Home Page Configuration.

= 2.1 =
* Remove Snow Effect.
* Update Theme Info Page with Premium Theme Features.

= 2.0 =
* Snow Effects.
* FA icons 4.5.0

= 1.4.8 =
* Minor Bug Fix.

= 1.4.7 =
* Menu fixes.

= 1.4.6 =
some Undefinded indexed fixed.

= 1.4.5 =
* Theme-Info added.
* Theme-Options Removed.

= 1.4.3 =
* Customizr Support Added.

= 1.4.2 =
* ALL POST linked to POST-PAGE.

= 1.4.1 =
* HOME-BLOG issue fixed.

= 1.4 =
* POST NAVIGATION.
* CHECKED(); implimented.

= 1.3.8 =
* checked() function used.
* WP QUERY used. 

= 1.3.7 =
* Comments li added.

= 1.3.6 =
* Removed Title Repetition HTML W3C Validates.
* Removed Style Tag from footer.
* Front-Page Static Options Fixed.

= 1.3.5 =
* Some minor HTML W3C validator fixes.

= 1.3.4 =
* Custom Static Front-Page.
* Title Repetition Removed.
* Slider Interval added. 

= 1.3.3 =
* Editor in Service Options in Theme Options.

= 1.3.2 =
* Repetitive Options-Settings.

= 1.3.1 =
* Snow-Effect .
* Minor CSS Update.

= 1.3 =
* Font-Awesome Bundled.
* Minor Date-Time func. Changes.

= 1.2 =
* Child theme Ready.
* Missing issues fixed.

= 1.1 =
* Add Sane Defaults data functionality.
* service icon name changes.

= 1.0 =
* Option Panel changed.
* Comment Function.
* Font Awesome 4.2.0 added.
 
= 0.9.6 =
* GPL License added for social media images and JS created by weblizar self.

= 0.9.5 =
* Minor BUg Fixed.

= 0.9.4 =
* Copyrights Attribution and TOS added on our own website.

= 0.9.3 =
* released.