<footer>
    <div class="footer">
    	<?php if(is_active_sidebar( 'footer-widget-area' )) { ?>
	        <div class="container">
	            <div class="footer-wrapper">
	                <div class="row">
						<?php  dynamic_sidebar( 'footer-widget-area' );  ?>
					</div>				
	            </div>
	        </div>
	    <?php } ?>
        <div class="copyright">
            <div class="container">
                <div class="row">
					<div class="col-md-6 col-sm-6">

                        <div class="copyright-text weblizar_footer_customizations">
                        	<?php if ( green_lantern_theme_is_companion_active() ) { ?>
	                        	<?php 
			                    $green_lantern_footer_customization = get_theme_mod( 'green_lantern_footer_customization', __('&copy; Copyright 2020. All Rights Reserved','green-lantern') );
			                    echo esc_html( $green_lantern_footer_customization ); ?>
								<a  rel="nofollow" href="<?php echo esc_url( get_theme_mod( 'green_lantern_deve_link' ) ); ?>" target="_blank">
								<?php  $green_lantern_develop_by = get_theme_mod( 'green_lantern_develop_by' );
			                       echo esc_html( $green_lantern_develop_by ); ?>
								</a>
							<?php 
				            }else{ ?>
				                <?php esc_html_e('&copy; Copyright 2020. All Rights Reserved','green-lantern'); ?>
				            <?php
				            } ?>
						</div>
                    </div>
                    <?php 
                    if ( green_lantern_theme_is_companion_active() ) {
		                $footer_section_social_media_enbled = absint(get_theme_mod('footer_section_social_media_enbled', 1));
		                if ($footer_section_social_media_enbled == 1) { ?>
		                    <div class="weblizar_social_media_enbled">
								<div class="col-md-6 col-sm-6"> 
									<div class="social-icons">
										<ul>
											<?php
				                            $fb_link = get_theme_mod('fb_link');
				                            if (!empty ($fb_link)) { ?>
												<li><a href="<?php echo esc_url(get_theme_mod('fb_link')); ?>" title="<?php esc_attr_e("Facebook",'green-lantern') ?>" target="_blank" class="social-media-icon facebook-icon"></a></li>
											<?php } 
											$twitter_link = get_theme_mod('twitter_link');
                            				if (!empty ($twitter_link)) { ?>
												<li><a href="<?php echo esc_url(get_theme_mod('twitter_link')); ?>" title="<?php esc_attr_e("Twitter",'green-lantern') ?>" target="_blank" class="social-media-icon twitter-icon"></a></li>
											
											<?php } 
											$linkedin_link = get_theme_mod('linkedin_link');
                            				if (!empty ($linkedin_link)) { ?>
												<li class="weblizar_linkedin_link"><a href="<?php echo esc_url(get_theme_mod('linkedin_link')); ?>" title="<?php esc_attr_e("Linkedin",'green-lantern') ?>" target="_blank" class="social-media-icon linkedin-icon"></a></li>
											<?php } ?>
										</ul>
									</div>
			                    </div>
							</div>
						<?php } 
            		} ?>
                </div>
            </div>
        </div>	
    </div>
</footer>
</div></div>
<!-- Return to Top -->
<?php if ( green_lantern_theme_is_companion_active() ) { 
    $green_lantern_return_top = absint(get_theme_mod( 'green_lantern_return_top', '1' ));
    if ( $green_lantern_return_top == "1" ) { ?>
		<a href="#" title="Go Top" class="top_scrollup"><i class="fa fa-chevron-up"></i></a>
	<?php } 
}?>
<?php wp_footer(); ?>
</body>
</html>