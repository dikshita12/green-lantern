<?php // Template Name: Home Template
get_header();

if ( green_lantern_theme_is_companion_active() ) {

    get_template_part('home', 'slider');

    if ($sections = json_decode(get_theme_mod('home_reorder'), true)) {
        foreach ($sections as $section) {
            $data = $section . "_home";
            if (get_theme_mod($data, 1) == "1") {
                get_template_part('home', $section);
            }
        }
    } else {

        get_template_part('home', 'service');

        $blog_home = absint(get_theme_mod('blog_home', 1));
        if ( $blog_home == 1) {
            get_template_part('home', 'blog');
        }
    }
} else { 
    get_template_part( 'no', 'content' );
}
get_footer();