jQuery(document).ready(function (jQuery) {
  
    //show until every thing loaded
    jQuery('.rev-slider-fixed,.rev-slider-full').css('visibility', 'visible');
    /* Full */
    if ( ajax_admin.ajax_data.side_pause == 'on') {
    jQuery('.rev-slider-banner-full').revolution({
        delay: ajax_admin.ajax_data.image_speed,
        startwidth: 1170,
        startheight: 500,
        
        pause: 'Hover',
        onHoverStop: ajax_admin.ajax_data.side_pause,
        

        thumbWidth: 100,
        thumbHeight: 50,
        thumbAmount: 3,

        hideThumbs: 0,

        navigationType: "none",
        navigationArrows: "solo",
        navigationStyle: "bullets",
        navigationHAlign: "center",
        navigationVAlign: "bottom",
        navigationHOffset: 30,
        navigationVOffset: 30,

        soloArrowLeftHalign: "left",
        soloArrowLeftValign: "center",
        soloArrowLeftHOffset: 20,
        soloArrowLeftVOffset: 0,

        soloArrowRightHalign: "right",
        soloArrowRightValign: "center",
        soloArrowRightHOffset: 20,
        soloArrowRightVOffset: 0,

        touchenabled: "on",

        stopAtSlide: -1,
        stopAfterLoops: -1,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        hideSliderAtLimit: 0,

        fullWidth: "on",
        fullScreen: "off",
        fullScreenOffsetContainer: "#topheader-to-offset",

        shadow: 0

    });
  }else{
    jQuery('.rev-slider-banner-full').revolution({
        delay: ajax_admin.ajax_data.image_speed,
        startwidth: 1170,
        startheight: 500,
        
        
        onHoverStop: ajax_admin.ajax_data.side_pause,
        

        thumbWidth: 100,
        thumbHeight: 50,
        thumbAmount: 3,

        hideThumbs: 0,

        navigationType: "none",
        navigationArrows: "solo",
        navigationStyle: "bullets",
        navigationHAlign: "center",
        navigationVAlign: "bottom",
        navigationHOffset: 30,
        navigationVOffset: 30,

        soloArrowLeftHalign: "left",
        soloArrowLeftValign: "center",
        soloArrowLeftHOffset: 20,
        soloArrowLeftVOffset: 0,

        soloArrowRightHalign: "right",
        soloArrowRightValign: "center",
        soloArrowRightHOffset: 20,
        soloArrowRightVOffset: 0,

        touchenabled: "on",

        stopAtSlide: -1,
        stopAfterLoops: -1,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        hideSliderAtLimit: 0,

        fullWidth: "on",
        fullScreen: "off",
        fullScreenOffsetContainer: "#topheader-to-offset",

        shadow: 0

    });
  }

    var mySwiper = new Swiper ('.swiper-container', {
      pagination: '.swiper-pagination',
      paginationClickable: true,

      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',

      // AutoPlay
     
      autoplay: ajax_admin.ajax_data.image_speed,
      
      // Loop
      loop: true,
      loopAdditionalSlides: 2,
      loopedSlides: 2,

      // Position
      slidesPerView: 'auto', //If "auto" or slidesPerView > 1, enable watchSlidesVisibility for lazy load

      // Keyboard and Mousewheel
      keyboardControl: true,
      mousewheelControl: true,
      mousewheelForceToAxis: true, // just use the horizontal axis to 

      // Lazy Loading 
      watchSlidesVisibility: true,
      preloadImages: false,
      lazyLoading: true,
    })
    if ( ajax_admin.ajax_data.side_pause == 'on' ) {
      jQuery('.swiper-container').on('mouseenter', function(e){
        console.log('stop autoplay');
        mySwiper.stopAutoplay();
    })
      jQuery('.swiper-container').on('mouseleave', function(e){
        console.log('start autoplay');
        mySwiper.startAutoplay();
      })
    } 

});