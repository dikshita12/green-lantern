jQuery(document).ready(function() {
	if( jQuery(window).width() > 767) {
	   jQuery('.nav li.dropdown').hover(function() {
		   jQuery(this).addClass('open');
	   }, function() {
		   jQuery(this).removeClass('open');
	   }); 
	   jQuery('.nav li.dropdown-submenu').hover(function() {
		   jQuery(this).addClass('open');
	   }, function() {
		   jQuery(this).removeClass('open');
	   }); 
	}
	
	jQuery('li.dropdown').find('.fa-angle-down').each(function(){
		jQuery(this).on('click', function(){
			if( jQuery(window).width() < 767) {
				jQuery(this).parent().next().slideToggle();
			}
			return false;
		});
	});
});
jQuery(document).ready(function() {
    jQuery(".navbar-nav").accessibleDropDown();
});

jQuery.fn.accessibleDropDown = function () {
  var el = jQuery(this);

  /* Make dropdown menus keyboard accessible */

    jQuery("a", el).focus(function() {
        jQuery(this).parents("li").addClass("force-show");
    }).blur(function() {
        jQuery(this).parents("li").removeClass("force-show");
    });
}

// scroll-up
jQuery(document).ready(function () {	
	jQuery(window).scroll(function () {
		if (jQuery(this).scrollTop() > 100) {
			jQuery('.top_scrollup').fadeIn();
		} else {
			jQuery('.top_scrollup').fadeOut();
		}
	});

	jQuery('.top_scrollup').click(function () {
		jQuery("html, body").animate({
			scrollTop: 0
		}, 600);
		return false;
	});

});	