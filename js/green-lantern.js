jQuery(document).ready(function ($) {
    //BLOG Slides
    var $blogFeaturesCarousel = $(".blog-features-carousel");
    $blogFeaturesCarousel.carouFredSel({
    responsive: true,
    prev: {
        button: ".feature-arrow-left",
        key: "left",
        items: 1
    },
    next: {
        button: ".feature-arrow-right",
        key: "right",
        items: 1
    },
    items: {
       width: 375,
        height: 450,
        visible: {
            min: 1,
            max: 3
        },
    },
    auto: false,
    scroll: 1,
    circular: false
    });
    
});
/* Centering Bullets */
function centeringBullets() {
    //Bullets center fixing in revolution slide
    jQuery('.simplebullets,.slider-fixed-frame .home-bullets').each(function () {
        var $this = $(this), w = $this.width();
        $this.css('margin-left', -(w / 2) + 'px');
    });
}