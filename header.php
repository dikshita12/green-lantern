<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">``
	<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />  
    <meta charset="<?php bloginfo('charset'); ?>" />	
	<?php wp_head(); ?>
</head>
<body <?php body_class(''); ?> id="bgpattern-p5" >
	<?php   
    if ( function_exists( 'wp_body_open' ) )
    wp_body_open();
    ?>
	<div class="boxed" id="wrapper">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'green-lantern' ); ?></a>
	<div id="top_wrapper">	
		<header id="header" >
			<div class="container">
				<div class="row">
					<?php 
					$green_lantern_search = absint(get_theme_mod( 'green_lantern_search', '1' ));
    				if ( $green_lantern_search == "1" ) { ?>
						<div class="col-md-12">
							<div class="col-md-4 top-search-form">
								<?php echo get_search_form(); ?>	
							</div>	
						</div>	
					<?php } ?>
					<nav class="navbar-default" role="navigation">
						<div class="container-fluid">						
							<div class="col-md-3">
								<div class="navbar-header">	
									<div class="logo pull-left">
										<?php if(has_custom_logo()) { the_custom_logo();} ?>
										<?php if (display_header_text()==true){  ?>
											<a title="Weblizar" href="<?php echo esc_url(home_url( '/' )); ?>">
												<?php 
												echo '<span class="site-title">'.esc_html(get_bloginfo('name')).'</span>'; 
												?>
											</a>	
										<?php 
										}
										if (display_header_text()==true){ ?>
											<p class="site-title-desc"><?php bloginfo( 'description' ); ?></p>
										<?php } ?>
									</div>
						 			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only"><?php esc_html_e('Toggle navigation','green-lantern') ?></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
								    </button>
								</div>
							</div>
							<div class="col-md-9">
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								    <?php
									wp_nav_menu( 
										array(  
											'theme_location' => 'primary',
											'container'   => 'nav-collapse collapse navbar-inverse-collapse',
											'menu_class'  => 'nav navbar-nav navbar-left',
											'fallback_cb' => 'wl_fallback_page_menu',
											'walker'      => new green_lantern_nav_walker()
											)
										);	
									?>
								</div>
							</div>
						</div>
					</nav>		
				</div>
			</div>
		</header>	
	</div>
	<div id="content" class="site-content">