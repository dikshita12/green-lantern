<div class="section-content section-alter">
	<div class="container">
		<?php 
	    $blog_title1 = get_theme_mod('blog_title', __('Home Blog Title','green-lantern'));
	    if ( ! empty ( $blog_title1 ) ) { ?>
			<div class="row">	
				<div class="col-md-12 col-sm-12">
					<div class="center-title">
						<div class="heading-title">
							<h2 class="h2-section-title weblizar_latest_post"><?php echo esc_html( $blog_title1); ?></h2>
						</div>
						<div class="space-sep20"></div>
					</div>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="blog-features-carousel">
				<?php 
	            $posts_count = wp_count_posts()->publish;
	           
	            $args = array('post_type' => 'post', 'posts_per_page' => $posts_count, 'ignore_sticky_posts' => 1);
	            
	            $post_type_data = new WP_Query($args);
	            if($post_type_data->have_posts()){
	            	$i=0;
	                while ($post_type_data->have_posts()):
	                    $post_type_data->the_post();
	                    ?>			
						<div data-animdelay="0.<?php echo esc_attr($i); ?>s" data-animspeed="1s" data-animrepeat="0" data-animtype="fadeIn" class="feature animated fadeIn animatedVisi" style="animation-delay: 0s; animation-duration: 1s;">
							<div class="feature-image img-overlay">
								<?php
								$defalt_arg =array('class' => "img-responsive");
								if(has_post_thumbnail()): 
								the_post_thumbnail('green_lantern_home_blog', $defalt_arg);
								$post_thumbnail_id = get_post_thumbnail_id();
								$image_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );					 
								?>					
								<div class="item-img-overlay">
									<div class="item_img_overlay_content">
										<a title="<?php the_title(); ?>"  href="<?php echo esc_url($image_thumbnail_url); ?>">
											<i class="fa fa-search"></i>
										</a>									
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><i class="fa fa-link"></i></a>                    
									</div>
								</div> 
								<?php endif; ?>
							</div>
							<div class="feature-content">
								<h3 class="h3-blog-title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</h3>
								<?php the_excerpt(); ?>
							</div>					
							<div class="feature-details">
								<span><i class="fa fa-picture-o"></i></span>							
								<span><i class="fas fa-clock"></i> <?php the_date(); ?></span>
								<span><i class="fa fa-user"></i><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"> <?php echo esc_html(get_the_author()); ?></a></span>
							</div>        
						</div>				
						<?php $i=$i+2;
					endwhile;
				}  ?>		
			</div>
		</div>
	</div>
	<div class="space-sep60"></div>
	<div class="feature-arrows">
		<div class="feature-arrow feature-arrow-left icon-angle-left"><i class="fa fa-arrow-left"></i></div>
		<div class="feature-arrow feature-arrow-right icon-angle-right"><i class="fa fa-arrow-right"></i></div>
	</div>
</div>