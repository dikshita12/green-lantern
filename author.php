<?php get_header(); ?>
<div class="top-title-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 page-info">
                <h1 class="h1-page-title"><?php 
				/* translators: %s: author name */
				printf( esc_html__( 'Author Archives: %s', 'green-lantern' ), the_author() ); ?></h1>				
            </div>
        </div>
    </div>
</div>
<div class="space-sep20"></div>	
<div class="content-wrapper">
	<div class="body-wrapper">
	    <div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-9">			
					<?php while(have_posts()):the_post();
						get_template_part( 'content', get_post_format() );
					endwhile; ?>				 
					<div class="text-center wl-theme-pagination">
				        <?php the_posts_pagination(); ?>
				        <div class="clearfix"></div>
				    </div>		
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>