<?php
/*	*Theme Name	: Green-Lantern
	*Theme Core Functions and Codes
*/	

	/* Get the plugin */
	if ( ! function_exists( 'green_lantern_theme_is_companion_active' ) ) {
	    function green_lantern_theme_is_companion_active() {
	        require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	        if ( is_plugin_active(  'weblizar-companion/weblizar-companion.php' ) ) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	}

	/**Includes reqired resources here**/
	require( get_template_directory() . '/class-tgm-plugin-activation.php' );
	require( get_template_directory() . '/core/menu/default_menu_walker.php' ); // for Default Menus
	require( get_template_directory() . '/core/menu/green_lantern_nav_walker.php' ); // for Custom Menus	
	require( get_template_directory() . '/core/comment-box/comment-function.php' ); //for comments
	require(get_template_directory()  . '/core/custom-header.php' );	

	add_action( 'after_setup_theme', 'green_lantern_setup' ); 	
	function green_lantern_setup()
	{	
		add_theme_support( 'title-tag' );
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
	    add_theme_support( 'wc-product-gallery-lightbox' );
	    add_theme_support( 'wc-product-gallery-slider' );
		global $content_width;
		//content width
		if ( ! isset( $content_width ) ) $content_width = 720; //px
	
		// Load text domain for translation-ready
		load_theme_textdomain( 'green-lantern' );	
		
		add_theme_support( 'post-thumbnails' ); //supports featured image
		// This theme uses wp_nav_menu() in one location.
		register_nav_menu( 'primary', __( 'Primary Menu', 'green-lantern' ) );
		// theme support 	
		$args = array('default-color' => 'ffffff',
		'default-image' => '',
		);
		add_editor_style();
		add_theme_support( 'custom-background', $args); 
		
		add_theme_support( 'automatic-feed-links'); 
		add_theme_support( 'customize-selective-refresh-widgets' );

		/* Gutenberg */
		add_theme_support( 'wp-block-styles' );
		add_theme_support( 'align-wide' );

		/* Add editor style. */
		add_theme_support( 'editor-styles' );
		add_theme_support( 'dark-editor-style' );

		
		// Logo
		add_theme_support( 'custom-logo', array(
			'width'       => 250,
			'height'      => 250,
			'flex-width'  => true,
			'flex-height'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		));

		add_theme_support( 'post-formats', array( 'gallery', 'quote', 'video', 'aside', 'image', 'link' ) );
			
	}

	function green_lantern_scripts()
	{	
		
		wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome-latest/css/all.css');
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
		wp_enqueue_style( 'carousel', get_template_directory_uri() . '/css/carousel.css');
		wp_enqueue_style( 'greenlantern-swiper', get_template_directory_uri() .'/css/swiper.css'); 
		wp_enqueue_style( 'green-lantern-green', get_template_directory_uri() . '/css/skins/green.css');
		wp_enqueue_style( 'green-lantern-theme-menu', get_template_directory_uri() . '/css/theme-menu.css');
		wp_enqueue_style('green-lantern-style-sheet', get_stylesheet_uri());
		wp_enqueue_style( 'green-lantern-responsive', get_template_directory_uri() . '/css/responsive.css');
		// Js
		
		wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() .'/js/menu/bootstrap.js', array( 'jquery' ), true, true);
		wp_enqueue_script('holder-js', get_template_directory_uri() .'/js/holder.js', array( 'jquery' ), true, true);
		wp_enqueue_script( 'green-lantern-menu', get_template_directory_uri() .'/js/menu/menu.js', array( 'jquery' ), true, true);
		
		if(is_front_page()) {
			wp_enqueue_script('jquery.themepunch.plugins', get_template_directory_uri() .'/js/jquery.themepunch.plugins.js', array( 'jquery' ), true, true);
			wp_enqueue_script('jquery.themepunch.revolution', get_template_directory_uri() .'/js/jquery.themepunch.revolution.js', array( 'jquery' ), true, true);
			wp_enqueue_script('jquery.caroufredsel-6.2.1-packed', get_template_directory_uri() .'/js/jquery.caroufredsel-6.2.1-packed.js', array( 'jquery' ), true, true);
			wp_enqueue_script('green-lantern', get_template_directory_uri() .'/js/green-lantern.js', array( 'jquery' ), true, true);
					
			wp_enqueue_style('revolution_settings', get_template_directory_uri() . '/css/revolution_settings.css');
			wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css');
			wp_enqueue_script('greenlantern-swiper', get_template_directory_uri() .'/js/swiper.js', array( 'jquery' ), true, true);
		

			$green_lantern_pause = absint(get_theme_mod( 'green_lantern_pause', '1' ));
	    	if ( $green_lantern_pause == "1" ) { 
	            $side_pause = 'on';
	        } else {
	            $side_pause ='off' ;
	        }

			$ajax_data = array(
	            'side_pause'  => $side_pause,
	            'image_speed' => absint(get_theme_mod( 'slider_image_speed', '2000' )),
	        );

	        wp_enqueue_script( 'gl-ajax-front', get_template_directory_uri() . '/js/gl-ajax-front.js', array( 'jquery' ), true, true );
	        wp_localize_script( 'gl-ajax-front', 'ajax_admin', array(
	                'ajax_url'    => admin_url( 'admin-ajax.php' ),
	                'admin_nonce' => wp_create_nonce( 'admin_ajax_nonce' ),
	                'ajax_data'   => $ajax_data,
	        ) );
	    }

		if ( is_singular() ) wp_enqueue_script( "comment-reply" ); 	
	}
	add_action('wp_enqueue_scripts', 'green_lantern_scripts'); 
	

	// Read more tag to formatting in blog page 
	function green_lantern_content_more($more)
	{  global $post;							
	   return '<div class="blog-post-details-item blog-read-more"><a href="'.esc_url(get_permalink()).'">Read More...</a></div>';
	}  

/*  Font Family */	
 add_action('wp_enqueue_scripts', 'green_lantern_font_family');
 function green_lantern_font_family()
   {
	 wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Rock+Salt|Neucha|Sans+Serif|Indie+Flower|Shadows+Into+Light|Dancing+Script|Kaushan+Script|Tangerine|Pinyon+Script|Great+Vibes|Bad+Script|Calligraffitti|Homemade+Apple|Allura|Megrim|Nothing+You+Could+Do|Fredericka+the+Great|Rochester|Arizonia|Astloch|Bilbo|Cedarville+Cursive|Clicker+Script|Dawning+of+a+New+Day|Ewert|Felipa|Give+You+Glory|Italianno|Jim+Nightshade|Kristi|La+Belle+Aurore|Meddon|Montez|Mr+Bedfort|Over+the+Rainbow|Princess+Sofia|Reenie+Beanie|Ruthie|Sacramento|Seaweed+Script|Stalemate|Trade+Winds|UnifrakturMaguntia|Waiting+for+the+Sunrise|Yesteryear|Zeyada|Warnes|Abril+Fatface|Advent+Pro|Aldrich|Alex+Brush|Amatic+SC|Antic+Slab|Candal');
     
	 wp_enqueue_style ('googleFonts');
    }
	
	add_filter( 'the_content_more_link', 'green_lantern_content_more' );
	
	/*
	* Weblizar widget area
	*/
	add_action( 'widgets_init', 'green_lantern_widgets_init');
	function green_lantern_widgets_init() {
		/*sidebar*/
		register_sidebar( array(
			'name' => __( 'Sidebar', 'green-lantern' ),
			'id'             => 'sidebar-primary',
			'description' => __( 'The primary widget area', 'green-lantern' ),
			'before_widget' => '<div id="%1$s" class="sidebar-block %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="h3-sidebar-title sidebar-title">',
			'after_title' => '</h3>'
		) );

		register_sidebar( array(
			'name' => __( 'Footer Widget Area', 'green-lantern' ),
			'id' => 'footer-widget-area',
			'description' => __( 'footer widget area', 'green-lantern' ),
			'before_widget' => '<div id="%1$s" class="col-md-3 col-sm-3 footer-col %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="footer-title">',
			'after_title' => '</div>',
		) );             
	}
	
/*
* Image resize and crop
*/
 if ( ( 'add_image_size' ) ) 
 { 
	add_image_size('green_lantern_media_blog_img',800,400,true);
}
	
//Plugin Recommend
add_action('tgmpa_register','green_lantern_plugin_recommend');
function green_lantern_plugin_recommend(){
	$plugins = array(
        array(
            'name'     => esc_html__('Weblizar Companion','green-lantern'),
            'slug'     => 'weblizar-companion',
            'required' => false,
        ),
    );
    tgmpa( $plugins );
}

add_action( 'wp_enqueue_scripts', 'green_lantern_custom_css' );
function green_lantern_custom_css() {
    $output = '';

	$output .= '.logo a img {
			  height: '.esc_attr(get_theme_mod('logo_height','55')).'px;
			  width : '.esc_attr(get_theme_mod('logo_width','150')).'px;
			}';

	//custom css
    $custom_css = get_theme_mod('custom_css') ; 
    if (!empty ($custom_css)) {
        $output .= get_theme_mod('custom_css') . "\n";
    }

    wp_register_style( 'custom-header-style', false );
    wp_enqueue_style( 'custom-header-style', get_template_directory_uri() . '/css/custom-header-style.css' );
    wp_add_inline_style( 'custom-header-style', $output );
}